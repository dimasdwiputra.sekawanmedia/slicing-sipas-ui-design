// Start Display Sidebar List
const nav = [
  {
    title: "Suartku",
    active: true,
    notif: "99",
    icon: `<svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke-width="1.5"
          stroke="currentColor"
          class="w-6 h-6"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            d="M9 3.75H6.912a2.25 2.25 0 00-2.15 1.588L2.35 13.177a2.25 2.25 0 00-.1.661V18a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18v-4.162c0-.224-.034-.447-.1-.661L19.24 5.338a2.25 2.25 0 00-2.15-1.588H15M2.25 13.5h3.86a2.25 2.25 0 012.012 1.244l.256.512a2.25 2.25 0 002.013 1.244h3.218a2.25 2.25 0 002.013-1.244l.256-.512a2.25 2.25 0 012.013-1.244h3.859M12 3v8.25m0 0l-3-3m3 3l3-3"
          />
        </svg>`,
  },
  {
    title: "Asistensi",
    active: false,
    notif: "99+",
    icon: `<svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke-width="1.5"
      stroke="currentColor"
      class="w-6 h-6"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        d="M9 17.25v1.007a3 3 0 01-.879 2.122L7.5 21h9l-.621-.621A3 3 0 0115 18.257V17.25m6-12V15a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 15V5.25m18 0A2.25 2.25 0 0018.75 3H5.25A2.25 2.25 0 003 5.25m18 0V12a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 12V5.25"
      />
    </svg>`,
  },
  {
    title: "Surat Unit",
    active: false,
    notif: "2",
    icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-6 h-6"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M21.75 6.75v10.5a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25m19.5 0v.243a2.25 2.25 0 01-1.07 1.916l-7.5 4.615a2.25 2.25 0 01-2.36 0L3.32 8.91a2.25 2.25 0 01-1.07-1.916V6.75"
              />
            </svg>`,
  },
  {
    title: "Pelaporan",
    active: false,
    icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-6 h-6"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M11.35 3.836c-.065.21-.1.433-.1.664 0 .414.336.75.75.75h4.5a.75.75 0 00.75-.75 2.25 2.25 0 00-.1-.664m-5.8 0A2.251 2.251 0 0113.5 2.25H15c1.012 0 1.867.668 2.15 1.586m-5.8 0c-.376.023-.75.05-1.124.08C9.095 4.01 8.25 4.973 8.25 6.108V8.25m8.9-4.414c.376.023.75.05 1.124.08 1.131.094 1.976 1.057 1.976 2.192V16.5A2.25 2.25 0 0118 18.75h-2.25m-7.5-10.5H4.875c-.621 0-1.125.504-1.125 1.125v11.25c0 .621.504 1.125 1.125 1.125h9.75c.621 0 1.125-.504 1.125-1.125V18.75m-7.5-10.5h6.375c.621 0 1.125.504 1.125 1.125v9.375m-8.25-3l1.5 1.5 3-3.75"
              />
            </svg>`,
  },
  {
    title: "Bantuan",
    active: false,
    icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-6 h-6"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z"
              />
            </svg>`,
    gapLine: `<div class="border-b text-white w-1/2"></div>`,
  },
];

navList(nav);

function navList(data) {
  const sidebar = document.getElementById("columns");

  for (let i = 0; i < data.length; i++) {
    let list = `
        ${data[i].gapLine ? data[i].gapLine : ""}
        <li
        class="w-full h-16 lg:h-12 xl:h-16 rounded-lg relative flex justify-center items-center text-white ${data[i].active ? "bg-white text-blue-900 font-semibold" : " hover:bg-white hover:bg-opacity-40 hover:text-white"} "
        >
            ${
              data[i].notif
                ? `<div
            class="w-8 h-4 lg:w-2 lg:h-2 xl:w-8 xl:h-4 bg-red-500 text-white rounded-full absolute top-0 right-0 mt-1 mr-1 flex justify-center items-center py-3 px-4 lg:p-0 xl:py-3 xl:px-4 text-[12px] font-semibold"
            >
            <p class="block lg:hidden xl:block">${data[i].notif}</p>
            </div>`
                : ""
            }
            <a href="#" class="flex flex-col justify-center items-center">
            <div class="">
                ${data[i].icon}
            </div>
            <p class="font-semibold text-sm hidden xl:block">${data[i].title}</p>
            </a>
        </li>
        `;
    sidebar.innerHTML += list;
  }
}
// End Display Sidebar List

// Start Display Menu List
    const menu = [
        {
            title: 'Tugas',
            active: false
        },
        {
            title: 'Masuk',
            active: false
        },
        {
            title: 'Koreksi',
            active: true
        },
        {
            title: 'Diteruskan',
            active: false
        },
    ]

    menuList(menu);

    function menuList(data) {
    const menu = document.getElementById("menu");

    for (let i = 0; i < data.length; i++) {
        let list = `
            ${data[i].active ? 
                `<li class="text-blue-500 border-b-4 border-solid border-blue-500 px-2 text-center">
                    ${data[i].title}
                </li>` 
                : 
                `<li class="text-gray-400">
                    ${data[i].title}
                </li>`}
            `;
        menu.innerHTML += list;
    }
}
// End Display Menu List

// Start Display Menu Items
const icon1 = `
    <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    fill="currentColor"

    >
        <path
        fill-rule="evenodd"
        d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
        clip-rule="evenodd"
        />
    </svg>
`
const icon2 = `
    <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    fill="currentColor"

    >
        <path
        fill-rule="evenodd"
        d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
        clip-rule="evenodd"
        />
    </svg>
`

const icon3 = `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
    <path fill-rule="evenodd" d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zm-1.72 6.97a.75.75 0 10-1.06 1.06L10.94 12l-1.72 1.72a.75.75 0 101.06 1.06L12 13.06l1.72 1.72a.75.75 0 101.06-1.06L13.06 12l1.72-1.72a.75.75 0 10-1.06-1.06L12 10.94l-1.72-1.72z" clip-rule="evenodd" />
    </svg>
`

const icon4 = `
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
    <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636" />
    </svg>
`

const menuItemsData = [
    {
        title: 'Penawaran Kerjasama dan Kemitraan',
        companyName: 'PT Indonesia jaya Merdeka',
        noReg: '# 055/DKA/06/2021',
        tipe: 'Penawaran',
        koreksi: 'Anda belum memberi tindakan',
        tanggalSurat: '12/05/22',
        keputusan: icon1,
        nilai: 'belum',
        active: true
    },
    {
        title: 'Penawaran Kerjasama dan Kemitraan',
        companyName: 'PT Indonesia jaya Merdeka',
        noReg: '# 055/DKA/06/2021',
        tipe: 'Penawaran',
        koreksi: 'Anda menyetujui pada tanggal 13/05/22',
        tanggalSurat: '12/05/22',
        keputusan: icon2,
        nilai: 'setuju',
        active: false
    },
    {
        title: 'Penawaran Kerjasama dan Kemitraan',
        companyName: 'PT Indonesia jaya Merdeka',
        noReg: '# 055/DKA/06/2021',
        tipe: 'Penawaran',
        koreksi: 'Anda menyetujui pada tanggal 13/05/22',
        tanggalSurat: '12/05/22',
        keputusan: icon2,
        nilai: 'setuju',
        active: false
    },
    {
        title: 'Penawaran Kerjasama dan Kemitraan',
        companyName: 'PT Indonesia jaya Merdeka',
        noReg: '# 055/DKA/06/2021',
        tipe: 'Penawaran',
        koreksi: 'Anda menolak pada 13/05/22',
        tanggalSurat: '12/05/22',
        keputusan: icon4,
        nilai: 'ditolak',
        active: false
    },
    {
        title: 'Penawaran Kerjasama dan Kemitraan',
        companyName: 'PT Indonesia jaya Merdeka',
        noReg: '# 055/DKA/06/2021',
        tipe: 'Penawaran',
        koreksi: 'Anda merevisi pada 13/05/22',
        tanggalSurat: '12/05/22',
        keputusan: icon3,
        nilai: 'revisi',
        active: false
    },
]

menuItems(menuItemsData)

function menuItems(data) {
    const menu = document.getElementById('items')

    for (let i = 0; i < data.length; i++) {
        let items = `
        <div class="grid grid-cols-6 py-4 lg:gap-4 px-4 ${data[i].active ? "bg-blue-50" : ""}">
            <div class="col-span-1 flex flex-col justify-start items-center gap-2">
                <div class="w-12 h-12 bg-[#584038] rounded-full flex justify-center items-center text-white">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        class="w-6 h-6"
                    >
                        <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M21.75 9v.906a2.25 2.25 0 01-1.183 1.981l-6.478 3.488M2.25 9v.906a2.25 2.25 0 001.183 1.981l6.478 3.488m8.839 2.51l-4.66-2.51m0 0l-1.023-.55a2.25 2.25 0 00-2.134 0l-1.022.55m0 0l-4.661 2.51m16.5 1.615a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V8.844a2.25 2.25 0 011.183-1.98l7.5-4.04a2.25 2.25 0 012.134 0l7.5 4.04a2.25 2.25 0 011.183 1.98V19.5z"
                        />
                    </svg>
                </div>
                <span class="text-xs font-bold w-full text-center text-[#584038]">Koreksi</span>
            </div>
            <div class="col-span-4">
                <p class="${data[i].active ? "font-semibold" : ""} text-base lg:text-sm xl:text-base">${data[i].title}</p>
                <span class="text-[13px] ${data[i].active ? "font-semibold" : ""}">${data[i].companyName}</span>
                <div class="flex justify-start items-center gap-3 mt-1">
                    <div class="border border-solid border-blue-600 px-2 xl:px-4 py-1 rounded-full text-blue-600 text-xs flex justify-center items-center">
                        ${data[i].noReg}
                    </div>
                    <div class="border border-solid border-blue-600 px-2 xl:px-4 py-1 rounded-full text-blue-600 text-xs">
                        ${data[i].tipe}
                    </div>
                </div>
                <div class="text-xs flex justify-start items-center mt-1 gap-1 ${data[i].nilai === 'belum' && 'text-yellow-400'} ${data[i].nilai === 'setuju' && 'text-green-600'} ${data[i].nilai === 'ditolak' | data[i].nilai === 'revisi' && 'text-red-600'}">
                    <div class="w-5 h-5">
                        ${data[i].keputusan}
                    </div>
                    <p class="font-semibold">${data[i].koreksi}</p>
                </div>
            </div>
            <div class="col-span-1 flex justify :nd justify-center elgitems-start">
                <span class="text-xs ${data[i].active ? "font-semibold" : ""}">${data[i].tanggalSurat}</span>
            </div>
        </div>
        `
        menu.innerHTML += items
    }
}
// End Display Menu Items